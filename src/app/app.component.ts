import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { PatientsService } from './services/patients.service';
import { Patient } from './models/patient';
import { MatDialog } from '@angular/material';
import { DialogComponent } from './dialog.component';

/**
 * @title Table with sorting
 */
@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.scss'],
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  displayedColumns: string[] = ['identifier', 'name', 'surname', 'birthDate'];
  dataSource: MatTableDataSource<Patient>;

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private patientsService: PatientsService,
              private matDialog: MatDialog) {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<Patient>(this.patientsService.patients);
    this.dataSource.sort = this.sort;
  }

  // TODO
  addPatient() {
    this.matDialog.open(DialogComponent, {
      width: '350px',
      height: '100%',
    });
    this.patientsService.addPatient(this.patientsService.patients[0]);
    this.dataSource.data = this.patientsService.patients;
  }
}
