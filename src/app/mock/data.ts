import { Patient } from '../models/patient';

export const FAKE_PATIENTS: Patient[] = [
  {identifier: 1, name: 'Pepe', surname: 'adasd', birthDate: new Date()},
  {identifier: 2, name: 'Juan', surname: 'adasd', birthDate: new Date()},
  {identifier: 3, name: 'Lithium', surname: 'adasd', birthDate: new Date()},
  {identifier: 4, name: 'Beryllium', surname: 'adasd', birthDate: new Date()},
  {identifier: 5, name: 'Boron', surname: 'adasd', birthDate: new Date()},
  {identifier: 6, name: 'Carbon', surname: 'adasd', birthDate: new Date()},
  {identifier: 7, name: 'Nitrogen', surname: 'adasd', birthDate: new Date()},
  {identifier: 8, name: 'Oxygen', surname: 'adasd', birthDate: new Date()},
  {identifier: 9, name: 'Fluorine', surname: 'adasd', birthDate: new Date()},
  {identifier: 10, name: 'Neon', surname: 'adasd', birthDate: new Date()},
];
