export interface Patient {
  name: string;
  identifier: number;
  surname: string;
  birthDate: Date;
}
